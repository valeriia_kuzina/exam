﻿namespace Exam
{
    public class RecordData
    {
        public int Place { get; private set; }
        public string Team { get; private set; }
        public string Name { get; private set; }
        public int Movements { get; private set; }

        public RecordData(int _place, string _team, string _name, int _movements)
        {
            Place = _place;
            Team = _team;
            Name = _name;
            Movements = _movements;
        }
    }
}
