﻿using System.Data.SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam
{
    class Database
    {
        
        private SQLiteConnection m_dbConn;
        private string fileName = "records.sqlite";

        public Database() 
        {
            if (!File.Exists(fileName))
                SQLiteConnection.CreateFile(fileName);


            m_dbConn = new SQLiteConnection($"Data Source = {fileName}; Version = 3; ");
            m_dbConn.Open();

            var command = m_dbConn.CreateCommand();
            command.CommandText =
            @"
                CREATE TABLE IF NOT EXISTS records (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, team TEXT NOT NULL, movementsCount INTEGER NOT NULL);
            ";
            command.ExecuteNonQuery();

        }

        public List<RecordData> SelectRecords()
        {

            var command = m_dbConn.CreateCommand();

            command.CommandText =
            @"
                SELECT team, name, movementsCount FROM records ORDER BY movementsCount ASC;
            ";

            List<RecordData> records = new List<RecordData>();

            using (var reader = command.ExecuteReader())
            {
                int place = 0;
                while (reader.Read())
                {
                    place++;
                    records.Add(new RecordData(place, reader.GetString(0), reader.GetString(1), reader.GetInt32(2)));
                }
            }

            return records;
        }

        public void InsertNewRecord(string name, string team, int movementsCount)
        {

            var command = m_dbConn.CreateCommand();

            command.CommandText =
            @"
                INSERT INTO records (name, team, movementsCount) VALUES ($name, $team, $movementsCount);
            ";

            command.Parameters.AddWithValue("$name", name).Size = 32;
            command.Parameters.AddWithValue("$team", team).Size = 32;
            command.Parameters.AddWithValue("$movementsCount", movementsCount);

            command.ExecuteNonQuery();

        }

        

    }
}
