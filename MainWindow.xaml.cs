﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Brush = System.Windows.Media.Brush;
using Brushes = System.Windows.Media.Brushes;

namespace Exam
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int FieldSizeHard = 10;
        const int FieldSizeMedium = 8;
        const int FieldSizeEasy = 6;

        private int currentFieldSize;
        private bool FieldGenerated = false;
        private bool firstPlayer = true;
        private bool mGameEnded;
        private int sheepMovementsCount;
        private GameFieldButton activeButton = null;

        private Database db;

        enum GameFieldStyle 
        {
            black_white,
            brown_beige
        }

        enum CharactersStyle
        { 
            classic,
            hd
        }


        public MainWindow()
        {
            InitializeComponent();
            db = new Database();
        }

        private void NewGame(int fieldSize)
        {
            currentFieldSize = fieldSize;
            firstPlayer = true;
            mGameEnded = false;
            activeButton = null;
            sheepMovementsCount = 0;

            RenderField();
            RenderCharacters();
        }

        private void RenderCharacters()
        {
            if (!FieldGenerated)
                return;

            RenderSheep();
            RenderWolves();
        }

        private void RenderWolves()
        {
            List<GameFieldButton> lastLine = GameField.Children.Cast<GameFieldButton>().Where(button => Grid.GetRow(button) == 0 && button.IsBlack).ToList();
            foreach (var item in lastLine)
            {
                item.character = GameFieldButton.Character.wolf;
                SetWolvesSkin();
            }
        }

        private void SetWolvesSkin()
        {
            Bitmap wolfSkin;

            if (MenuItem_CharantersSkin_HD.IsChecked)
            {
                if (MenuItem_FieldStyleBlackWhite.IsChecked)
                    wolfSkin = Properties.Resources.BlackWolfHD;
                else
                    wolfSkin = Properties.Resources.BWolfHD;
            }
            else
            {
                if (MenuItem_FieldStyleBlackWhite.IsChecked)
                    wolfSkin = Properties.Resources.BlackWolf;
                else
                    wolfSkin = Properties.Resources.BWolf;
            }

            ImageBrush skin = new ImageBrush(Imaging.CreateBitmapSourceFromHBitmap(wolfSkin.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions()));

            List<GameFieldButton> wolves = GameField.Children.Cast<GameFieldButton>().Where(button => button.character == GameFieldButton.Character.wolf && button.IsBlack).ToList();
            foreach (var item in wolves)
            {
                item.Background = skin;
            }

            wolfSkin.Dispose();
        }

        private void RenderSheep()
        {
            List<GameFieldButton> firstLine = GameField.Children.Cast<GameFieldButton>().Where(button => Grid.GetRow(button) == currentFieldSize - 1 && button.IsBlack).ToList();
            int randomButton = new Random().Next(0, firstLine.Count);

            firstLine[randomButton].character = GameFieldButton.Character.sheep;
            SetSheepSkin();
        }

        private void SetSheepSkin()
        {
            Bitmap sheepSkin;

            if (MenuItem_CharantersSkin_HD.IsChecked)
            {
                if (MenuItem_FieldStyleBlackWhite.IsChecked)
                    sheepSkin = Properties.Resources.BlackSheepHD;
                else
                    sheepSkin = Properties.Resources.BSheepHD;
            }
            else
            {
                if (MenuItem_FieldStyleBlackWhite.IsChecked)
                    sheepSkin = Properties.Resources.BlackSheep;
                else
                    sheepSkin = Properties.Resources.BSheep;
            }

            ImageBrush skin = new ImageBrush(Imaging.CreateBitmapSourceFromHBitmap(sheepSkin.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions()));
           
            GameFieldButton sheep = GameField.Children.Cast<GameFieldButton>().Where(button => button.character == GameFieldButton.Character.sheep && button.IsBlack).First();
            sheep.Background = skin;
            sheepSkin.Dispose();
        }

        private void RenderField()
        {
            FieldGenerated = true;

            GameField.ColumnDefinitions.Clear();
            GameField.RowDefinitions.Clear();
            GameField.Children.Clear();

            for (int i = 0; i < currentFieldSize; i++)
            {
                GameField.ColumnDefinitions.Add(new ColumnDefinition());
                GameField.RowDefinitions.Add(new RowDefinition());
            }

            for (int i = 0; i < currentFieldSize; i++)
            {
                bool isBlack = i % 2 == 1;
                for (int j = 0; j < currentFieldSize; j++)
                {
                    GameFieldButton bt = new GameFieldButton();
                    bt.Click += OnGameFieldClicked;

                    bt.IsBlack = isBlack;

                    GameField.Children.Add(bt);
                    Grid.SetRow(bt, i);
                    Grid.SetColumn(bt, j);
                    isBlack = !isBlack;
                }
            }


            UpdateFieldStyle();
        }

        private void UpdateFieldStyle()
        {
            Bitmap firstCell;
            Bitmap secondCell;

            if (MenuItem_FieldStyleBrownBeige.IsChecked)
            {
                firstCell = Properties.Resources.brownCell;
                secondCell = Properties.Resources.beigeCell;
            }
            else 
            {
               firstCell = Properties.Resources.blackCell;
               secondCell = Properties.Resources.whiteCell;
            }

            ImageBrush fc = new ImageBrush(Imaging.CreateBitmapSourceFromHBitmap(firstCell.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions()));
            ImageBrush sc = new ImageBrush(Imaging.CreateBitmapSourceFromHBitmap(secondCell.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions()));

           GameField.Children.Cast<GameFieldButton>().ToList().ForEach(bt => bt.Background = bt.IsBlack ? fc : sc);

            firstCell.Dispose();  //удаление
            secondCell.Dispose();
        }

        private void OnGameFieldClicked(object sender, RoutedEventArgs e)
        {
            if (!FieldGenerated)
                return;

            if (mGameEnded)
            {
                NewGame(currentFieldSize);
                return;
            }

            var button = (GameFieldButton)sender;

            if (activeButton == null)
            {
                if ((firstPlayer && button.character == GameFieldButton.Character.wolf)
                    || (!firstPlayer && button.character == GameFieldButton.Character.sheep))
                {
                    activeButton = button;
                    button.BorderBrush = Brushes.Red;
                    return;
                }
                
            }
            else
            {
                var origColomn = Grid.GetColumn(activeButton);
                var origRow = Grid.GetRow(activeButton);

                var column = Grid.GetColumn(button);
                var row = Grid.GetRow(button);


                if (firstPlayer)
                {
                    if (Math.Abs(origColomn - column) == 1 && row - origRow == 1
                        && button.character == GameFieldButton.Character.none)
                    {
                        activeButton.character = GameFieldButton.Character.none;
                        activeButton.BorderBrush = null;
                        activeButton.Content = "";
                        Brush bg = button.Background;
                        button.Background = activeButton.Background;
                        activeButton.Background = bg;


                        button.character = GameFieldButton.Character.wolf;
                        firstPlayer = !firstPlayer;

                        activeButton = null;
                        //picture

                        CheckWinWolves();
                    }


                }
                else
                {
                    if (Math.Abs(origColomn - column) == 1 && Math.Abs(row - origRow) == 1
                        && button.character == GameFieldButton.Character.none)
                    {
                        activeButton.character = GameFieldButton.Character.none;
                        activeButton.BorderBrush = null;
                        activeButton.Content = "";
                        Brush bg = button.Background;
                        button.Background = activeButton.Background;
                        activeButton.Background = bg;
                        activeButton = null;

                        button.character = GameFieldButton.Character.sheep;
                        firstPlayer = !firstPlayer;
                        sheepMovementsCount++;
                        CheckForWinSheep();
                    }
                }
            }
        }

        private void CheckWinWolves()
        {
            GameFieldButton sheep = GameField.Children.Cast<GameFieldButton>().Where(button => button.character == GameFieldButton.Character.sheep && button.IsBlack).First();

            var column = Grid.GetColumn(sheep);
            var row = Grid.GetRow(sheep);

            List<GameFieldButton> patentialWays = GameField.Children.Cast<GameFieldButton>().Where(button =>
            button.IsBlack &&
            Math.Abs(column - Grid.GetColumn(button)) == 1 && Math.Abs(row - Grid.GetRow(button)) == 1 &&
            button.character == GameFieldButton.Character.none).ToList();

            if (patentialWays.Count == 0)
            {
                mGameEnded = true;
                MessageBox.Show("Wolves win!");
                db.InsertNewRecord("PlayerWolf", "Волки", sheepMovementsCount);
                return;
            }
        }

        private void CheckForWinSheep()
        {
            // sheep win
            GameFieldButton sheep = GameField.Children.Cast<GameFieldButton>().Where(button => button.character == GameFieldButton.Character.sheep && button.IsBlack).First();

            if (Grid.GetRow(sheep) == 0)
            {
                mGameEnded = true;
                MessageBox.Show("Sheep win!", "Победа");
                db.InsertNewRecord("PlayerSheep", "Овца", sheepMovementsCount);
                return;
            }
           
        }

        private void OnFieldStyleBrownBeigeChecked(object sender, RoutedEventArgs e)
        {
            if (!FieldGenerated)
                return;

            MenuItem_FieldStyleBlackWhite.IsChecked = false;
            MenuItem_FieldStyleBrownBeige.IsChecked = true;
            UpdateFieldStyle();
            SetWolvesSkin();
            SetSheepSkin();
        }

        private void OnFieldStyleBlackWhiteChecked(object sender, RoutedEventArgs e)
        {
            if (!FieldGenerated)
                return;

            MenuItem_FieldStyleBrownBeige.IsChecked = false;
            MenuItem_FieldStyleBlackWhite.IsChecked = true;
            UpdateFieldStyle();
            SetWolvesSkin();
            SetSheepSkin();
        }

        private void OnNewGameEasy(object sender, RoutedEventArgs e)
        {
            NewGame(FieldSizeEasy);
        }

        private void OnNewGameMedium(object sender, RoutedEventArgs e)
        {
            NewGame(FieldSizeMedium);
        }

        private void OnNewGameHard(object sender, RoutedEventArgs e)
        {
            NewGame(FieldSizeHard);
        }

        private void OnMenuItem_CharacterSkin_Classic_Clicked(object sender, RoutedEventArgs e)
        {
            if (!FieldGenerated)
                return;

            MenuItem_CharantersSkin_Classic.IsChecked = true;
            MenuItem_CharantersSkin_HD.IsChecked = false;

            SetWolvesSkin();
            SetSheepSkin();
        }

        private void OnMenuItem_CharacterSkin_HD_Clicked(object sender, RoutedEventArgs e)
        {
            if (!FieldGenerated)
                return;

            MenuItem_CharantersSkin_Classic.IsChecked = false;
            MenuItem_CharantersSkin_HD.IsChecked = true;

            SetWolvesSkin();
            SetSheepSkin();
        }

        private void OnRecordsClicked(object sender, RoutedEventArgs e)
        {
            new Window1(db.SelectRecords()).Activate();
        }
    }
}
